import sys
import math
from pk.models import *
from geopy import distance
from django.contrib.gis.geos import Point
from django.contrib.gis.geos import LineString

def intersect_point_to_line(point, line_start, line_end):
    from django.contrib.gis.geos import Point
    line_magnitude =  line_end.distance(line_start)
    u = ((point.x - line_start.x) * (line_end.x - line_start.x) + (point.y - line_start.y) * (line_end.y - line_start.y)) / (line_magnitude ** 2)
    if u < 0.00001 or u > 1:
        ix = point.distance(line_start)
        iy = point.distance(line_end)
        if ix > iy:
            return line_end
        else:
            return line_start
    else:
        ix = line_start.x + u * (line_end.x - line_start.x)
        iy = line_start.y + u * (line_end.y - line_start.y)
        return Point(ix, iy, srid=4326)

points = Points.objects.all()
routes = Routes.objects.all()
pks    = Pks.objects.all()

point_to_route = []

for point in points:
    min_dist = sys.maxint
    for route in routes:
        tmp = point.the_geom.distance(route.the_geom)
        if tmp < min_dist:
            min_dist = tmp
            nearest_route = route
    point_to_route.append({'point':point, 'route': nearest_route, 'distance': min_dist})

for rel in point_to_route:
    min_dist = sys.maxint
    for route_section in rel['route'].the_geom.coords:
        for index in range(len(route_section)):
            route_point = route_section[index]
            prev_point = route_section[index-1] if index > 0 else None
            next_point = route_section[index+1] if index < len(route_section)-1 else None
            tmp_point = Point(route_point[0], route_point[1], , srid=4326)
            route_section_point_distance = rel['point'].the_geom.distance(tmp_point)
            if min_dist > route_section_point_distance:
                min_dist                  = route_section_point_distance
                projection_point          = tmp_point
                projection_point_next     = next_point
                projection_point_previous = prev_point
    rel['projection_point']          = projection_point
    rel['projection_distance']       = min_dist
    rel['projection_point_next']     = projection_point_next
    rel['projection_point_previous'] = projection_point_previous

for rel in point_to_route:
    projection = Projections(point_gid = rel['point'].gid, route_gid=rel['point'].gid, the_geom = LineString(rel['point'].the_geom, rel['projection_point']))
    projection.save()

for rel in point_to_route:
    ref_point       = rel['point'].the_geom
    projected_point = rel['projection_point']
    next_point      = Point(rel['projection_point_next'][0], rel['projection_point_next'][1], srid=4326) if rel['projection_point_next'] != None else None
    previous_point  = Point(rel['projection_point_previous'][0], rel['projection_point_previous'][1], srid=4326) if rel['projection_point_previous'] != None else None
    dist_1 = None
    dist_2 = None
    if next_point != None:
        intersect_1 = intersect_point_to_line(ref_point, projected_point, next_point)
        dist_1 = ref_point.distance(intersect_1)
    if previous_point != None:
        intersect_2 = intersect_point_to_line(ref_point, projected_point, previous_point)
        dist_2 = ref_point.distance(intersect_2)
    if dist_1 != None and dist_2 != None:
        if dist_1 < dist_2:
            final_projection = intersect_1
        else:
            final_projection = intersect_2
    elif dist_1 != None:
        final_projection = intersect_1
    else:
        final_projection = intersect_2
    projection = OrthoProjections(point_gid = rel['point'].gid, route_gid=rel['point'].gid, the_geom = LineString(rel['point'].the_geom, final_projection))
    projection.save()
    projection_point = OrthoProjectionPoints(point_gid = rel['point'].gid, route_gid=rel['point'].gid, the_geom = final_projection)
    projection_point.save()
    rel['orthoprojection_point'] = final_projection
    rel['orthoprojection_point_id'] = projection_point.id

distance.VincentyDistance.ELLIPSOID = 'WGS-84'
d = distance.distance

for rel in point_to_route:
    current_pks = Pks.objects.filter(nom_voie=rel['route'].nom, o_f='O')
    nb_pk = len(current_pks)
    if nb_pk > 0:
        for route_section in rel['route'].the_geom:
            found_distance = False
            section_start  = Point(route_section[0], srid=4326)
            section_end    = Point(route_section[-1], srid=4326)
            found_section  = False
            section_pk     = None
            distance_points = []
            projected_distance = 0
            section_points     = route_section
            for pk in current_pks:
                section_pk = pk
                if pk.the_geom.distance(section_start) < 0.00001:
                    found_section = True
                    section_previous_point = Point(section_points[0])
                    break
                elif pk.the_geom.distance(section_end) < 0.00001:
                    found_section = True
                    section_points.reverse()
                    section_previous_point = Point(section_points[0], srid=4326)
                    break
            if found_section:
                distance_points.append(section_pk.the_geom)
                for section_point in section_points:
                    section_point_pt = Point(section_point, srid=4326)
                    distance_points.append(section_point_pt)
                    projected_distance = projected_distance + d(section_previous_point, section_point_pt).m
                    if rel['projection_point'].distance(section_point_pt) < 0.00001:
                        distance_to_hp = d(rel['projection_point'], rel['orthoprojection_point']).m
                        if d(section_previous_point,section_point_pt) > d(section_previous_point,rel['orthoprojection_point']):
                            projected_distance  = projected_distance - distance_to_hp
                            distance_points.pop()
                        else:
                            projected_distance  = projected_distance + distance_to_hp
                        found_distance = True
                        rel['pk_distance']  = projected_distance
                        rel['reference_pk'] = section_pk
                        distance_points.append(rel['orthoprojection_point'])
                        pk_obj = PkDistances(orthopoint=OrthoProjectionPoints.objects.get(id=rel['orthoprojection_point_id']), refpk=section_pk, distance=projected_distance, the_geom=LineString(distance_points))
                        pk_obj.save()
                        rel['pk_distance_obj'] = pk_obj
                        break
                    section_previous_point = section_point_pt
            if found_distance:
                break
    else:
        rel['pk_distance']  = None
        rel['reference_pk'] = None
# use this query to export the results as csv
#copy (select p.gid, p.data_id, op.id as "orthopoint id", ds.distance as "distance in meter", pk.nom_voie as "road name", pk.km as "pk value in km", (ds.distance/1000)+pk.km as "cumulative distance in km", ds.id as "pk distance id" from points p inner join orthoprojectionpoint op on op.point_gid = p.gid inner join pkdistances ds on ds.orthopoint_id = op.id inner join pks pk on pk.gid = ds.refpk_id) to '/tmp/result_pk.csv' with csv;